#pragma once
#include <string>
#include <iostream>

using std::string;

class Activity {

private:
	string title;
	string type;
	string description;
	int time;

public:
	/*
	Constructor
	*/
	Activity(const string title, const string type, const string description, int time) : title{ title }, type{ type }, description{ description }, time{ time }{}
	
	Activity() = default;

	/*
	Returns the type of the activity
	*/
	string getType() const noexcept{

		return type;

	}

	/*
	Returns the type of the activity
	*/
	string getTitle() const noexcept{
		
		return title;
	
	}

	/*
	Returns the time of the activity
	*/
	int getTime() const noexcept {
	
		return time;
	
	}

	/*
	Returns the description of the activity
	*/
	string getDescription() const {
		return description;
	}


	/*
	Changes the actual description with the given one
	parametres: description - string( the new one )
	*/
	void setDescription(const string& description) {

		this->description = description;

	}

	/*
	Changes the actual time of the activity with the given one
	parametres: time - integer ( the new one )
	*/
	void setTime(const int time) noexcept {

		this->time = time;

	}

	/*
	Changes the actual type of the activity
	parametres: type - string( the new one )
	*/
	void setType(const string& type) {

		this->type = type;

	}

	/*
	Dictates the way an activity is written in a file
	*/
	string toString() {

		return title + " " + type + " " + description + " " + std::to_string(time);

	}
};
