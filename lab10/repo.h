#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "domain.h"
#include <ostream>
#include <unordered_map>
#include <cstdlib>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <fstream>


using std::vector;
using std::string;
using std::ostream;


class Repository {

public:
	virtual void store(const Activity& element) = 0;

	virtual const Activity& find(string title) const = 0;
	
	virtual const vector<Activity> getAll() noexcept = 0;
	
	virtual void modify(const Activity& element, const string& type, const string& description, const int time) = 0;
	
	virtual void to_delete(const Activity& element) = 0;
	
	virtual void empty_list()=0;

};

class ListRepository :public Repository {

private:
	
	std::vector<Activity>list;

	bool exist(const Activity& element)const;


public:

	ListRepository() = default;

	ListRepository(const ListRepository& other_ListRepository) = delete;

	/*
	Adds an element to the list
	element - element of type Activity
	Throws exception if the element already exists
	*/
	void store(const Activity& element)override;

	/*
	Searches for an element by title
	Throws exception if the element is not found
	*/
	const Activity& find(string title) const override;

	/*
	Returns the list of elements
	*/
	const vector<Activity> getAll() noexcept override;

	/*
	Modifies an element given
	parametres: description - string, time - integer , type - string
	Throws exception if element is not found
	*/
	void modify(const Activity& element, const string& type, const string& description, const int time) override;


	/*
	Deletes an element from the list
	Throws exception if element is not found
	*/
	void to_delete(const Activity& element) override;

	/*
	Deletes all the elements from the list
	*/
	virtual void empty_list();


};



class FileRepository : public ListRepository {
private:
	
	string filename;
	void loadAllFromFile();
	void writeAllToFile();

public:
	FileRepository(const string& filename) :filename{ filename }{

		loadAllFromFile();
	
	}

	FileRepository() = default;
	
	FileRepository(const FileRepository& other_FileRepository) = delete;

	/*
	Deletes all data from the file and from the list
	*/
	void empty_list() override;
	
	/*
	Adds element to file
	Throws exception if element already exists
	*/
	void store(const Activity& element) override;

	/*
	Modifies an element
	parametres: description - string, time - integer, type - string
	Throws exception is element is not in file
	*/
	void modify(const Activity& element, const string& type, const string& description, const int time) override ;

	/*
	Deletes an element from file
	Throws exception if element is not in file
	*/
	void to_delete(const Activity& element)override ;


};

/*
Used for the exceptional situations that might appear in repository 
*/
class RepositoryException {
private:
	
	string message;

public:
	
	RepositoryException(string m) :message{ m } {}

	friend ostream& operator<<(ostream& out, const RepositoryException& ex);

};

ostream& operator<<(ostream& out, const RepositoryException& ex);

void testeRepo();