#include "lab10.h"
#include <QtWidgets/QApplication>
#include "service.h"
#include "domain.h"
#include "repo.h"
#include "validator.h"
#include "listaActuala.h"
#include "Gui.h"
#include "new.h"

void testAll() {
	testeRepo();
	testCtr();
	testValidator();
	testCurrentList();
}

int main(int argc, char* argv[])
{
	testAll();
	

	QApplication a(argc, argv);

	FileRepository repo{ "activitati.txt"};
	Validator val;
	CurrentList lista_aux;
	Service ctr{ repo ,val,lista_aux };
	GUI gui{ ctr };
	
	gui.show();


	return a.exec();
    return 0;

}




