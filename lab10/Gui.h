#pragma once
#include "service.h"
#include "domain.h"
#include <QtWidgets/qwidget.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qlistwidget.h>
#include <QtWidgets/qformlayout.h>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qpushbutton.h>
#include <qmessagebox.h>
#include <QtWidgets/qcombobox.h>
#include <QTWidgets/qgridlayout.h>
#include <QDebug>
#include <QRadioButton>
#include <QComboBox>
#include <QTabWidget>
#include <QStackedLayout>
#include <QCloseEvent>
#include "COSgui.h"
#include "QTableWidget"


class GUI : public QWidget {
private:
	Service& service;
	QListWidget* mainList = new QListWidget;
	QListWidget* filterList = new QListWidget;
	QListWidget* sortList = new QListWidget;
	QListWidget* additionalInformation = new QListWidget;


	//buttons
	vector<QPushButton*> buttons;

	QPushButton* btnExit = new QPushButton{ "&Exit" };
	QPushButton* btnAdd = new QPushButton{ "&Add" };
	QPushButton* btnDelete = new QPushButton{ "&Delete" };
	QPushButton* btnModify = new QPushButton{ "&Modify" };
	QPushButton* btnFilterByType = new QPushButton{ "&Filter Type" };
	QPushButton* btnFilterByDescription = new QPushButton{ "&Filter Description" };
	QPushButton* btnSortTitle = new QPushButton{ "&Sort Title" };
	QPushButton* btnSortDescription = new QPushButton{ "&Sort Description" };
	QPushButton* btnSortTypeTime = new QPushButton{ "&Sort Type-Time" };
	QPushButton* undo = new QPushButton{ "&Undo" };
	QPushButton* btnCancel = new QPushButton{"&Cancel"};
	QPushButton* btnSave = new QPushButton{ "&Save" };
	QPushButton* btnClose = new QPushButton{ "&Close" };
	QPushButton* btnAsk = new QPushButton{"&Ask a Question"};
	QPushButton* btnSaveFilter = new QPushButton{ "&Save changes from filtres" };
	QPushButton* btnSaveSort = new QPushButton{ "&Save changes from sorting" };
	QPushButton* btnSend = new QPushButton{ "&Send" };
	QPushButton* btnDetails = new QPushButton{ "&View Details" };
	QPushButton* btnAddActivityToCurrentList = new QPushButton{"&Make A Current List"};
	
	QRadioButton* radioBtnTerms = new QRadioButton("I agree with terms and conditions");
	QRadioButton* radioBtnTermsAlso = new QRadioButton("I also agree with these terms and conditions");

	QLineEdit* txtFilname = new QLineEdit;
	QLineEdit* txtNumber = new QLineEdit;
	QLineEdit* txtTitle = new QLineEdit;
	QLineEdit* txtDescription = new QLineEdit;
	QLineEdit* txtTime = new QLineEdit;
	QLineEdit* txtType = new QLineEdit;
	QLineEdit* txtFilter = new QLineEdit;
	QLineEdit* txtAsk = new QLineEdit;

	QComboBox* filterComboBox = new QComboBox;
	QComboBox* sortComboBox = new QComboBox;

	void initGUI();
	void connectSignalsToSlots();
	void loadInfo(const string& filename);
	void reloadList(const vector<Activity>& list);

public:
	/*
	Explicit Constructor
	*/
	GUI(Service& srv) :service{ srv } {

		initGUI();
		connectSignalsToSlots();
		reloadList(service.getAll());
		loadInfo("precizari.txt");

	}

	/*
	Event required when the button close(X) is pushed
	event - specified QCloseEvent
	*/
	void GUI::closeEvent(QCloseEvent* event);
};

	