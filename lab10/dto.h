#pragma once
#include <string>
using std::string;

class DTO {
private:
	string type;
	int value;

public:
	/*
	Explicit Constructor
	*/
	DTO(const string tip, int val) : type{ tip }, value{ val }{};

	/*
	Returns the type of the DTO
	*/
	string getType() const {

		return type;

	}

	/*
	Returns the value of the DTO
	*/
	int getValue() const noexcept {

		return value;

	}

	/*
	Sets a new value for the DTO
	parametres: value - integer( the adding )
	*/
	void setValue(int value) noexcept {

		this->value += value;

	}


};