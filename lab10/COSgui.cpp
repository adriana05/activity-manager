#include "COSgui.h"
#include <QStyleFactory>
#include <QSpinBox>
#include <qtablewidget.h>
#include <QLabel>

void GuiActualList::initFirstWdw() {
	
	qDeleteAll(children());
	service.addOsberver(this);

	setFixedHeight(400);
	setFixedWidth(500);


	QVBoxLayout* layoutMain = new QVBoxLayout;
	setLayout(layoutMain);
	setStyle(QStyleFactory::create("Fusion"));

	QPalette darkPalette;
	darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::WindowText, Qt::white);
	darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
	darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
	darkPalette.setColor(QPalette::ToolTipText, Qt::white);
	darkPalette.setColor(QPalette::Text, Qt::white);
	darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::ButtonText, Qt::white);
	darkPalette.setColor(QPalette::BrightText, Qt::red);
	darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));



	darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
	darkPalette.setColor(QPalette::HighlightedText, Qt::black);

	setPalette(darkPalette);

	setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
	
	lstSrv = new QListWidget;
	listActivities = new QListWidget;
	loadList(listActivities, service.get_all_list());
	loadList(lstSrv, service.getAll());

	QPushButton* btnNext = new QPushButton{ "&Next" };
	QPushButton* btnAdd = new QPushButton{ "&->" };
	QPushButton* btnUndo = new QPushButton{ "&<-" };
	QPushButton* btnGoleste = new QPushButton{ "&Empty list" };
	QPushButton* btnRandom = new QPushButton{ "&Random generator" };
	QPushButton* btnDetails = new QPushButton{ "&View Details" };

	QSpinBox* spinBox = new QSpinBox;

	auto layout1 = new QHBoxLayout;
	auto layout2 = new QVBoxLayout;
	auto layout3 = new QHBoxLayout;
	auto layout4 = new QHBoxLayout;
	auto layout5 = new QVBoxLayout;
	auto layout6 = new QVBoxLayout;



	layout3->addWidget(btnRandom);
	layout3->addWidget(spinBox);
	layout3->addSpacing(400);
	layout3->addWidget(btnDetails);
	layoutMain->addLayout(layout3);

	auto label = new QLabel("Full List of Activities");
	layout5->addWidget(label);
	layout5->addWidget(lstSrv);

	layout1->addLayout(layout5);

	layout2->addSpacing(150);
	layout2->addWidget(btnAdd);
	layout2->addWidget(btnUndo);
	layout2->addWidget(btnGoleste);
	layout2->addSpacing(150);



	layout1->addLayout(layout2);

	auto form1 = new QLabel("Today's list of activities");
	layout6->addWidget(form1);
	layout6->addWidget(listActivities);
	
	layout1->addLayout(layout6);

	layoutMain->addLayout(layout1);
	layout4->addSpacing(500);
	layout4->addWidget(btnNext);
	layoutMain->addLayout(layout4);


	QObject::connect(btnNext, &QPushButton::clicked, [&]() {
		initSecodWdw();
		});

	QObject::connect(lstSrv, &QListWidget::itemSelectionChanged, [&]() {
		if (lstSrv->selectedItems().isEmpty()) {
			title = "";
		}
		else {
			QListWidgetItem* selItem = lstSrv->selectedItems().at(0);
			auto str = (selItem->text());
			title = str.toStdString();
		}
		});
	QObject::connect(btnAdd, &QPushButton::clicked, [&]() {
		if (title=="") {
			return;
		}
		try {
				service.addActivitateLista(title);
				loadList(listActivities, service.get_all_list());
			}
		catch (RepositoryException&)
			{
				QMessageBox::information(nullptr, "Info", "Exista deja in lista!");
			}
			});

	QObject::connect(btnUndo, &QPushButton::clicked, [&]() {
		if (service.get_all_list().size() == 0) {
			QMessageBox::warning(nullptr, "Warning", "Deja lista e vida!");
		}
		else {
			service.undo();
			loadList(listActivities, service.get_all_list());
		}
		});

	QObject::connect(btnGoleste, &QPushButton::clicked, [&]() {
		service.goleste_lista();
		loadList(listActivities, service.get_all_list());
		});

	QObject::connect(btnRandom, &QPushButton::clicked, [&]() {
		if (random > service.getAll().size()) {
			QMessageBox::warning(nullptr, "Warning", "We cannot generate your wishes, Sir!");
		}
		else if (random == 0) {
			service.goleste_lista();
			loadList(listActivities, service.get_all_list());
		}
		else {
			service.genereaza_lista(random);
			loadList(listActivities, service.get_all_list());
		}
		});
	QObject::connect(spinBox, QOverload<int>::of(&QSpinBox::valueChanged),[&](int i) { 
		random = i;
		});


	QObject::connect(btnDetails, &QPushButton::clicked, [&]() {
		int nrRows = service.getAll().size();
		QTableWidget* tbl = new QTableWidget{ nrRows , 4 };
		auto fereastra = new QWidget{};
		fereastra->setFixedHeight(500);
		fereastra->setFixedWidth(550);
		auto vector = service.getAll();
		int i = 0;
		for (auto item : vector) {
			tbl->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(item.getTitle())));
			tbl->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(item.getType())));
			tbl->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(item.getDescription())));
			int number = item.getTime();
			string str_number = std::to_string(number);
			tbl->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(str_number)));
			i++;
		}
		auto layout = new QVBoxLayout{};
		layout->addWidget(tbl);
		fereastra->setLayout(layout);

		fereastra->show();
		});
}

void GuiActualList::initSecodWdw() {

	qDeleteAll(children());

	setFixedHeight(200);
	setFixedWidth(500);
	QVBoxLayout* layoutMain = new QVBoxLayout;
	//delete layoutMain;
	setLayout(layoutMain);
	
	QPushButton* btnBack = new QPushButton{ "&Back" };
	QPushButton* btnExport = new QPushButton{ "&Export" };
	QPushButton* btnClose = new QPushButton{ "&Close" };

	auto layout2 = new QHBoxLayout;
	auto layout3 = new QHBoxLayout;


	txtFilename = new QLineEdit;
	auto form = new QFormLayout;
	auto label = new QLabel("Introduce the File Name where you want to save your today's list of activities");
	form->addRow(txtFilename);
	layoutMain->addWidget(label);
	layoutMain->addLayout(form);

	layout2->addSpacing(500);
	layout2->addWidget(btnExport);

	layoutMain->addLayout(layout2);

	layout3->addSpacing(500);
	layout3->addWidget(btnBack);
	layout3->addWidget(btnClose);

	layoutMain->addLayout(layout3);

	QObject::connect(btnBack, &QPushButton::clicked, [&]() {
		initFirstWdw();
		});

	QObject::connect(btnClose, &QPushButton::clicked, [&]() {
		close();
		});
	QObject::connect(btnExport, &QPushButton::clicked, [&]() {
		auto str = txtFilename->text();
		string to_str = str.toStdString();
		try {
			service.export_files(to_str);
			QMessageBox::information(nullptr, "Info", "SUCCESS!");

		}
		catch (RepositoryException&) {
			QMessageBox::warning(nullptr, "Warning", "Could not open the file!");
		}
		});

}




void GuiActualList::loadList(QListWidget* lst, const vector<Activity>& all) {
	lst->clear();
	for (auto& a : all) {
		lst->addItem(QString::fromStdString(a.getTitle()));
	}
}

void GuiActualList::closeEvent(QCloseEvent* event) {

	auto ret = QMessageBox::warning(nullptr, tr("Warning!"), tr("Are you sure you want to exit without saving ?"), QMessageBox::Yes | QMessageBox::Save | QMessageBox::No);
	if (ret == QMessageBox::Yes) {

		service.goleste_lista();
		event->accept();

	}
	else if (ret == QMessageBox::No) {

		event->ignore();

	}
	else {

		event->accept();

	}
}

void GuiActualList::update() {

	loadList(lst, service.get_all_list());

}