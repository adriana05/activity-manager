#include "domain.h"
#include "repo.h"
#include "listaActuala.h"


class UndoAction {
public:
	virtual void doUndo() = 0;
	virtual ~UndoAction() = default;
};

class UndoAdauga : public UndoAction {
private:
	Activity activ_adaugata;
	Repository& rep;
public:
	UndoAdauga(Repository& rep, Activity& activ_adauga) : rep{ rep }, activ_adaugata{ activ_adauga }{}

	/*
	Sterge ultima activitate adaugata in lista de activitati
	*/
	void doUndo() override {
		rep.to_delete(activ_adaugata);
	}
};

class UndoSterge :public UndoAction {
private:
	Activity activ_stergere;
	Repository& rep;
public:
	UndoSterge(Repository& rep, Activity& activ) :rep{ rep }, activ_stergere{ activ } {}

	/*
	Adauga activitatea staarsa anterior din lista de activitati
	*/
	void doUndo()override {
		rep.store(activ_stergere);
	}
};

class UndoModifica :public UndoAction {
private:
	Activity activ_modificata;
	Activity activ_initiala;
	Repository& rep;
public:
	UndoModifica(Repository& rep, Activity& activ_noua, Activity& activ_veche) :rep{ rep }, activ_modificata{ activ_noua }, activ_initiala{ activ_veche } {}

	/*
	Reface starea anterioara a unei activitati ce a suferit modificari
	*/
	void doUndo() override {
		rep.modify(activ_modificata, activ_initiala.getType(), activ_initiala.getDescription(), activ_initiala.getTime());
	}
};

class UndoAddListaActuala : public UndoAction {
private:
	CurrentList& lista;
	Activity activ_adaugata;
public:
	UndoAddListaActuala(CurrentList& lista, const Activity& a) : lista{ lista }, activ_adaugata{ a }{}
	/*
	Sterge ultima activitate introdusa in lista de activitati actuale
	*/
	void doUndo() override {
		lista.to_delete(activ_adaugata);
	}

};
