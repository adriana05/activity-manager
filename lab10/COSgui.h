#pragma once
#include "service.h"
#include "domain.h"
#include <QtWidgets/qwidget.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qlistwidget.h>
#include <QtWidgets/qformlayout.h>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qpushbutton.h>
#include <qmessagebox.h>
#include <QtWidgets/qcombobox.h>
#include <QTWidgets/qgridlayout.h>
#include <QDebug>
#include <QRadioButton>
#include <QComboBox>
#include <QTabWidget>
#include <QStackedLayout>
#include <QCloseEvent>

class GuiActualList : public QWidget,public Observer  {
private:

	QHBoxLayout* layoutMain = new QHBoxLayout{};
	QListWidget* lstSrv ;
	Service& service;
	QListWidget* lst = new QListWidget;
	string title;
	QListWidget* listActivities ;
	QLineEdit* txtFilename;

	int random;
	
	void initFirstWdw();
	void initSecodWdw();
	void loadList(QListWidget* lst, const vector<Activity>& list);

public:
	/*
	Explicit Constructor
	*/
	GuiActualList(Service& reached_service) :service{ reached_service } {
		
		initFirstWdw();
		setLayout(layoutMain);

	}

	/*
	Updates the list when notified
	*/
	void update() override;

	/*
	Event required when the button close(X) is pushed
	event - specified QCloseEvent 
	*/
	void GuiActualList::closeEvent(QCloseEvent* event);

	/*
	Explicit Destrucotr(Destroyer)
	*/
	~GuiActualList() {

		service.removeObserver(this);

	}
};