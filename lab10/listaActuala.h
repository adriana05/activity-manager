#pragma once
#include <iostream>
#include <vector>
#include "domain.h"
#include "repo.h"
#include <random>
#include <chrono>
#include <assert.h>
#include <iterator>
#include "Observer.h"

class CurrentList : public Observable {

private:

	std::vector<Activity> all_elements;

public:
	CurrentList() = default;

	/*
	Returns all the elements from the list
	*/
	std::vector<Activity> getAll();

	/*
	Clears all the list and notifies the change
	*/
	void clear_list();
	

	/*
	Adds a new element to the list
	Throws exception if the element already exists in the list
	*/
	void add(const Activity& element);

	/*
	Deletes an element from the list
	parametres: element - element Type given to be searched and deleted
	*/
	void to_delete(const Activity& element);
	
	/*
	Shuffles the list given and returns it as a result
	*/
	vector<Activity>& shuffle(vector<Activity>& fullList);

	/*
	Populates the current list whth a number of elements given
	parametres: numberElemnts - integer, fullList - vector of elemnts already in file
	*/
	void populate(unsigned int numberElements, vector<Activity>& fullList);

};


void testCurrentList();