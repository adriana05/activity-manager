#pragma once
#include "Observer.h"
#include <qpainter.h>
#include <qwidget.h>
#include "service.h"
#include <random>
#include <time.h>

class Painter : public Observer, public QWidget {
private:
	
	Service& srv;

public:
	Painter(Service& srv) : srv{ srv } {

		srv.addOsberver(this);
	
	}

	/*
	Update of the painter when notified a change was made
	*/
	void update()override {
	
		repaint();
	
	}
	
	/*
	Event required when changes happen to the currnet list of activities
	*/
	void paintEvent(QPaintEvent* ev) override {
	
		QPainter p{this};
		srand(time(NULL));

		for (const auto& a : srv.get_all_list()) {
			int x = rand() % 500, y = rand() % 400, w = rand() % 150, h = rand() % 150;
			//p.drawRect(x, y, w, h);
			p.drawPie(x, y, w, h, 50, 500);

		}

	}
};

