#include "listaActuala.h"


std::vector<Activity> CurrentList::getAll() {

	return all_elements;

}

/*
Clears all the list and notifies the change
*/
void CurrentList::clear_list() {

	all_elements.clear();
	notify();

}

/*
Adds a new element to the list
Throws exception if the element already exists in the list
*/
void CurrentList::add(const Activity& element) {

	for (auto existing_element : all_elements) {

		if (existing_element.getTitle() == element.getTitle())
			throw RepositoryException("The element is already in the list");

	}

	all_elements.push_back(element);

	notify();

}

/*
Deletes an element from the list
parametres: element - element Type given to be searched and deleted
*/
void CurrentList::to_delete(const Activity& element) {

	std::vector<Activity>::iterator iteratorList;

	for (iteratorList = all_elements.begin(); iteratorList < all_elements.end(); iteratorList++) {

		if ((*iteratorList).getTitle() == element.getTitle())
		{
			all_elements.erase(iteratorList);
			notify();
			return;
		}

	}
}

/*
Shuffles the list given and returns it as a result
*/
vector<Activity>& CurrentList::shuffle(vector<Activity>& fullList) {

	auto  seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(fullList.begin(), fullList.end(), std::default_random_engine(seed));

	return fullList;
}

/*
Populates the current list whth a number of elements given
parametres: numberElemnts - integer, fullList - vector of elemnts already in file
*/
void CurrentList::populate(unsigned int numberElements, vector<Activity>& fullList) {

	shuffle(fullList);
	clear_list();

	for (auto element : fullList) {

		if (numberElements == 0)
			break;
		add(element);
		numberElements--;

	}

	notify();

}

void testCurrentList()
{
	CurrentList l;
	assert(l.getAll().size() == 0);
	Activity a{ "fotbal","sportiv","super",10 };
	l.add(a);
	assert(l.getAll().size() == 1);
	Activity b{ "fotbal","sportiv","ok",432 };
	try {
		l.add(b);
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
	l.clear_list();
	assert(l.getAll().size() == 0);

	l.add(a);
	l.to_delete(a);
	assert(l.getAll().size() == 0);

	vector<Activity> repo;
	repo.push_back(a);
	Activity c{ "baschet","sportiv","superr",45 };
	Activity d{ "cititi","cultural","exceptional",124 };
	repo.push_back(c);
	repo.push_back(d);
	l.populate(2, repo);
	assert(l.getAll().size() == 2);

}