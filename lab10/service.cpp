#include "Service.h"
#include <functional>
#include <assert.h>
#include <iterator> 
#include <map> 
#include <vector>
#include <iostream> 
#include "dto.h"
#include <algorithm>
#include <random>
#include <chrono>
#include <fstream>
#include <numeric>

using std::vector;
using std::string;


const vector<Activity> Service::getAll() noexcept {
	return rep.getAll();
}

vector<DTO> Service::rapoarte() {
	std::map<string, int>dict;
	std::map<string, int>::iterator itr;
	vector<Activity> v{ rep.getAll() };//fac o copie	
	vector<DTO>vec;
	for (std::vector<Activity>::iterator i = v.begin(); i < v.end(); i++) {
		itr = dict.find((*i).getType());
		if (itr != dict.end())
			itr->second += 1;
		else {
			dict.insert(std::pair<string, int>((*i).getType(), 1));

		}
	}
	for (itr = dict.begin(); itr != dict.end(); ++itr) {
		DTO d{ itr->first ,itr->second };
		vec.push_back(d);
	}

	auto sum = std::accumulate(vec.begin(), vec.end(), 0, [](int sum, const DTO& a) {return sum + a.getValue(); });
	return vec;
}


/*
Sorteaza dupa titlu
*/

vector<Activity> Service::sortByTitle() {
	vector<Activity>sorted{ rep.getAll() };
	std::sort(sorted.begin(), sorted.end(), [](const Activity& a1, const Activity& a2) {
		return a1.getTitle() < a2.getTitle();
		});
	return sorted;
}


//Sorteaza dupa description

vector<Activity> Service::sortByDescription() {
	vector<Activity>sorted{ rep.getAll() };
	std::sort(sorted.begin(), sorted.end(), [](const Activity& a1, const Activity& a2) {
		return a1.getDescription() < a2.getDescription();
		});
	return sorted;
}



//Sorteaza dupa tip+durata

vector<Activity> Service::sortByTypeTime() {
	vector<Activity>sorted{ rep.getAll() };
	std::sort(sorted.begin(), sorted.end(), [](const Activity& a1, const Activity& a2) {
		if (a1.getType() == a2.getType()) {
			return a1.getTime() < a2.getTime();
		}
		return a1.getType() < a2.getType();
		});
	return sorted;

}



/*
Adauga o activitate
arunca exceptie daca: nu se poate salva, nu este valida
*/
void Service::addActivitate(const string& title, const string& type, const string& description, int time) {
	Activity a{ title, type, description, time };
	val.validate(a);
	rep.store(a);

	undoActions.push_back(std::make_unique<UndoAdauga>(rep, a));
	notify();
	
}

/*
Modifica o activitate
arunca exeptie daca nu exista activitatea care se cauta pentru a fi modificata
Date de intrare: titlu,tup,descriere,duarat
*/

void Service::modifyActivitate(const string& title, const string& type, const string& description, const int time) {
	Activity activity = rep.find(title);
	rep.modify(activity, type, description, time);
	Activity activity_new = rep.find(title);
	undoActions.push_back(std::make_unique<UndoModifica>(rep, activity_new, activity));
}

/*
Cauta activitate dupa titlu si tip si o returneaza;
Date de intrare: 2 string -titlu, tip
*/

const Activity Service::findActivity(const string& title)const {
	Activity activ = rep.find(title);
	return activ;
}


vector<Activity> Service::filtrareType(const string& type) {
	vector<Activity>rez;
	vector<Activity>lista_intreaga = rep.getAll();
	std::copy_if(lista_intreaga.begin(), lista_intreaga.end(), std::back_inserter(rez), [type](const Activity& a) {
		return a.getType() == type;
		});
	return rez;
}

vector<Activity> Service::filtrareDescriere(const string& description) {
	vector<Activity>rez;
	vector<Activity>lista_intreaga = rep.getAll();
	std::copy_if(lista_intreaga.begin(), lista_intreaga.end(), std::back_inserter(rez), [description](const Activity& a) {
		return description == a.getDescription();
		});
	return rez;
}


void Service::deletee(const string& title) {
	Activity a = rep.find(title);
	rep.to_delete(a);

	undoActions.push_back(std::make_unique<UndoSterge>(rep, a));
	notify();
}

vector<Activity> Service::addActivitateLista(const string& title) {

	Activity a = rep.find(title);
	curr_list.add(a);

	undoActions.push_back(std::make_unique<UndoAddListaActuala>(curr_list, a));
	return curr_list.getAll();
}

void Service::goleste_lista() {
	curr_list.clear_list();
}

int Service::genereaza_lista(unsigned int numar) {
	auto vector_init = rep.getAll();

	if (numar > vector_init.size() || numar <= 0) {
		return -1;
	}
	curr_list.populate(numar, vector_init);
	return 0;
}

void Service::export_files(const string& filename)
{
	std::ofstream out(filename);
	if (!out.is_open()) {
		//verify if the stream is opened
		std::string msg("Error open file");
		throw RepositoryException(msg);
	}

	auto lista_pentru_fisier = curr_list.getAll();
	for (auto& activs : lista_pentru_fisier) {
		out << activs.getTitle() << "," << activs.getType() << "," << activs.getDescription() << "," << activs.getTime() << std::endl;
	}
	out.close();
}

void Service::undo() {
	if (undoActions.empty())
		throw RepositoryException("\nNo more undo\n");
	undoActions.back()->doUndo();
	undoActions.pop_back();
	notify();
}

void testAdaugaCtr() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service ctr{ rep,val,lista_aux };
	ctr.addActivitate("a", "a", "b", 6);
	assert(ctr.getAll().size() == 1);
	ctr.addActivitateLista("a");
	assert(ctr.get_all_list().size() == 1);

	try {
		ctr.addActivitateLista("b");
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
	//adaug ceva invalid
	try {
		ctr.addActivitate("", "", "", -1);
		assert(false);
	}
	catch (ValidateException&) {
		assert(true);
	}
	//incerc sa adaug ceva ce existadeja
	try {
		ctr.addActivitate("a", "a", "b", 6);
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
}
void testModifica() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service ctr{ rep,val,lista_aux };
	ctr.addActivitate("ana", "are", "mere", 1);
	ctr.modifyActivitate("ana", "are", "pere", 2);
	Activity a = rep.find("ana");
	assert(a.getDescription() == "pere");
	assert(a.getTime() == 2);
}

void testCautare() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service srv{ rep,val,lista_aux };
	srv.addActivitate("ana", "are", "mere", 1);
	Activity a = srv.findActivity("ana");
	assert(a.getDescription() == "mere");

}

void testStergere() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service ctr{ rep,val,lista_aux };
	ctr.addActivitate("ana", "are", "mere", 1);
	ctr.addActivitateLista("ana");
	ctr.deletee("ana");
	ctr.goleste_lista();
	assert(rep.getAll().size() == 0);
	assert(lista_aux.getAll().size() == 0);
}

void testFiltrari() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service srv{ rep,val,lista_aux };
	srv.addActivitate("ana", "are", "meree", 1);
	srv.addActivitate("anaa", "are", "mereeee", 1);
	srv.addActivitate("anaaa", "nu_are", "mere", 1);
	srv.addActivitate("anaaaaa", "n-are", "mere", 1);
	srv.addActivitate("anaaaaaaa", "a", "mere", 1);
	assert(srv.filtrareDescriere("mere").size() == 3);
	assert(srv.filtrareType("are").size() == 2);

}

void testSortare() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service srv{ rep,val,lista_aux };
	srv.addActivitate("c", "d", "c", 9);
	srv.addActivitate("b", "a", "b", 14);
	srv.addActivitate("a", "a", "a", 10);
	auto a = srv.sortByTitle().at(0);
	assert(a.getTitle() == "a");
	a = srv.sortByDescription().at(0);
	assert(a.getDescription() == "a");
	a = srv.sortByTypeTime().at(0);
	assert(a.getTime() == 10);



}
void testRaport() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service srv{ rep,val,lista_aux };
	srv.addActivitate("fotbal", "sport", "ok", 1);
	srv.addActivitate("volei", "sport", "simaiok", 2);
	srv.addActivitate("citit", "cultura", "superok", 3);
	vector<DTO>vec = srv.rapoarte();
	assert(vec.size() == 2);

	assert(vec.at(0).getType() == "cultura");
	assert(vec.at(0).getValue() == 1);
	assert(vec.at(1).getType() == "sport");
	assert(vec.at(1).getValue() == 2);


}

void testRandom() {
	ListRepository rep;
	Validator val;
	CurrentList lista_aux;
	Service srv{ rep,val,lista_aux };
	srv.addActivitate("c", "d", "c", 9);
	srv.addActivitate("b", "a", "b", 14);
	srv.addActivitate("a", "a", "a", 10);

	assert(srv.genereaza_lista(-1) == -1);
	assert(srv.genereaza_lista(10) == -1);

	srv.genereaza_lista(2);
	assert(lista_aux.getAll().size() == 2);
}

void testUndo() {
	ListRepository rep;
	CurrentList lista_auxiliara;
	Validator val;
	Service srv{ rep, val, lista_auxiliara };

	srv.addActivitate("fotbal", "sportiv", "ok", 90);
	srv.addActivitate("citit", "cultural", "super", 120);

	srv.addActivitate("baschet", "sporivt", "fun", 50);
	assert(rep.getAll().size() == 3);
	srv.undo();
	assert(rep.getAll().size() == 2);

	srv.deletee("fotbal");
	assert(rep.getAll().size() == 1);
	srv.undo();
	assert(rep.getAll().size() == 2);

	srv.modifyActivitate("citit", "educativ", "yusss", 180);
	assert(rep.find("citit").getType() == "educativ");
	srv.undo();
	assert(rep.find("citit").getType() == "cultural");

	srv.addActivitateLista("citit");
	srv.undo();
	assert(lista_auxiliara.getAll().size() == 0);

}

void testCtr() {
	testAdaugaCtr();
	testModifica();
	testCautare();
	testStergere();
	testFiltrari();
	testSortare();
	testRaport();
	testRandom();
	testUndo();
}

