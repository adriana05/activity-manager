#include "Gui.h"
#include "service.h"
#include "domain.h"
#include <QtWidgets/qwidget.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qlistwidget.h>
#include <QtWidgets/qformlayout.h>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qpushbutton.h>
#include <qmessagebox.h>
#include <QtWidgets/qcombobox.h>
#include <QTWidgets/qgridlayout.h>
#include <QDebug>
#include <QRadioButton>
#include <QComboBox>
#include <QTabWidget>
#include <QStackedLayout>
#include <QCloseEvent>
#include <vector>
#include <fstream>
#include <sstream>
#include <QStyleFactory>
#include <qapplication.h>
#include <QLabel>
#include "histogram.h"



void GUI::initGUI() {

	
	
	QHBoxLayout* layoutMain = new QHBoxLayout{};
	setLayout(layoutMain);

	QTabWidget* tabs = new QTabWidget(this);
	QWidget* tab1 = new QWidget;
	QWidget* tab2 = new QWidget;
	QWidget* tab3 = new QWidget;
	tabs->addTab(tab1, "File");
	tabs->addTab(tab2, "Filter/Sort");
	tabs->addTab(tab3, "Info");

	


	auto layout2 = new QVBoxLayout{};
	auto layout3 = new QVBoxLayout{};
	auto layout4 = new QHBoxLayout{};
	auto layout5 = new QVBoxLayout{};
	auto layout6 = new QHBoxLayout{};
	auto layout7 = new QVBoxLayout{};
	


	layoutMain->addWidget(tabs);
	tab1->setLayout(layout7);


	auto form = new QFormLayout;
	form->addRow("Title", this->txtTitle);
	form->addRow("Type", txtType);
	form->addRow("Description", txtDescription);
	form->addRow("Time", txtTime);

	auto buttonLyaout = new QHBoxLayout;
	auto buttonLyaout2 = new QHBoxLayout;
	buttonLyaout->addWidget(btnAdd);
	buttonLyaout->addWidget(btnDelete);
	buttonLyaout->addWidget(btnModify);

	buttonLyaout2->addWidget(undo);
	buttonLyaout2->addSpacing(105);
	buttonLyaout2->addWidget(btnDetails);

	layout2->addLayout(form);
	layout2->addLayout(buttonLyaout);
	layout2->addLayout(buttonLyaout2);

	auto lstLabel = new QLabel("Full list of activities");
	layout5->addWidget(lstLabel);
	layout5->addWidget(mainList);

	layout6->addLayout(layout2);
	layout6->addLayout(layout5);
	layout7->addLayout(layout6);


	

	auto forming = new QFormLayout;
	forming->addRow("Filename", txtFilname);
	forming->addRow("Number", txtNumber);
	layout4->addLayout(forming);

	auto newstuff = new QHBoxLayout;
	newstuff->addWidget(btnAddActivityToCurrentList);
	newstuff->addSpacing(400);
	newstuff->addWidget(btnExit);

	filterComboBox->addItem("Filtreaza dupa tip");
	filterComboBox->addItem("Filtreaza dupa descriere");

	layout7->addLayout(newstuff);


	auto layout8 = new QVBoxLayout;
	auto lay8 = new QHBoxLayout;
	auto layout9 = new QHBoxLayout;
	auto layout10 = new QVBoxLayout;
	auto layout11 = new QVBoxLayout;



	auto forms = new QFormLayout;
	layout10->addWidget(filterComboBox);
	forms->addRow("Filter type/description", txtFilter);
	layout10->addLayout(forms);
	auto filterLabel = new QLabel("Filtered list");
	layout10->addWidget(filterLabel);
	layout10->addWidget(filterList);
	


	sortComboBox->addItem("Sort Title");
	sortComboBox->addItem("Sort Type-Time");
	sortComboBox->addItem("Sort Description");

	layout11->addWidget(sortComboBox);
	auto sortLabel = new QLabel("Sorted list");
	layout11->addWidget(sortLabel);
	
	layout11->addWidget(sortList);
	

	lay8->addSpacing(150);
	lay8->addWidget(btnSaveFilter);
	lay8->addSpacing(150);
	lay8->addWidget(btnSaveSort);


	layout9->addLayout(layout10);
	layout9->addLayout(layout11);
	
	layout8->addLayout(layout9);
	layout8->addLayout(lay8);

	tab2->setLayout(layout8);

	auto layout12 = new QHBoxLayout;
	layout3->addWidget(additionalInformation);
	auto newform = new QFormLayout;
	layout3->addWidget(radioBtnTerms);
	layout3->addWidget(radioBtnTermsAlso);

	newform->addRow("Ask us a question? ",txtAsk);
	layout12->addLayout(newform);
	layout12->addWidget(btnSend);

	layout3->addLayout(layout12);
	tab3->setLayout(layout3);


	qApp->setStyle(QStyleFactory::create("Fusion"));

	QPalette darkPalette;
	darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::WindowText, Qt::white);
	darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
	darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
	darkPalette.setColor(QPalette::ToolTipText, Qt::white);
	darkPalette.setColor(QPalette::Text, Qt::white);
	darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
	darkPalette.setColor(QPalette::ButtonText, Qt::white);
	darkPalette.setColor(QPalette::BrightText, Qt::red);
	darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

	darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
	darkPalette.setColor(QPalette::HighlightedText, Qt::black);

	qApp->setPalette(darkPalette);

	qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");

}
void GUI::connectSignalsToSlots() {
	QObject::connect(btnExit, &QPushButton::clicked, [&]() {
		close();
		});

	QObject::connect(btnAdd, &QPushButton::clicked, [&]() {
		auto title = txtTitle->text();
		auto type = txtType->text();
		auto description = txtDescription->text();
		auto time = txtTime->text();
		try {
			service.addActivitate(title.toStdString(), type.toStdString(), description.toStdString(), time.toInt());
			reloadList(service.getAll());
		}
		catch (ValidateException& v) {
			QMessageBox::warning(nullptr, "WARNING", "EROARE DE VALIDARE");

		}
		catch (RepositoryException&) {
			QMessageBox::information(nullptr, "Info", "EXista deja aceasta activitate BOII");
		}
		});

	QObject::connect(btnDelete, &QPushButton::clicked, [&]() {
		auto title = txtTitle->text();
		try {
			service.deletee(title.toStdString());
			reloadList(service.getAll());

		}
		catch (RepositoryException&) {
			QMessageBox::warning(nullptr, "Info", "Nu exista aceasta activitatea, nu cred ca stii ce faci!");
		}
		});
	QObject::connect(btnModify, &QPushButton::clicked, [&]() {
		auto title = txtTitle->text();
		auto type = txtType->text();
		auto description = txtDescription->text();
		auto time = txtTime->text();
		try {
			service.modifyActivitate(title.toStdString(), type.toStdString(), description.toStdString(), time.toInt());
			reloadList(service.getAll());
		}
		catch (RepositoryException&) {
			QMessageBox::warning(nullptr, "WARNING", "Nu exista o astfel de activitate");

		}
		});
	
	QObject::connect(mainList, &QListWidget::itemSelectionChanged, [&]() {
		if (mainList->selectedItems().isEmpty()) {
			//nu este nimic selectat (golesc detaliile)
			txtTitle->setText("");
			txtType->setText("");
			txtDescription->setText("");
			txtTime->setText("");
			return;
		}
		QListWidgetItem* selItem = mainList->selectedItems().at(0);
		txtTitle->setText(selItem->text());
		auto cautat = service.findActivity(selItem->text().toStdString());
		txtDescription->setText(QString::fromStdString(cautat.getDescription()));
		txtType->setText(QString::fromStdString(cautat.getType()));
		txtTime->setText(QString::number(cautat.getTime()));
		});

	
	QObject::connect(undo, &QPushButton::clicked, [&]() {
		try {
			service.undo();
			reloadList(service.getAll());

		}
		catch (RepositoryException&) {
			QMessageBox::warning(nullptr, "WARNING", "NO MORE UNDO !");
		}

		});


	QObject::connect(filterComboBox, QOverload<int>::of(&QComboBox::activated), [&](int index) {
		if (filterComboBox->currentIndex() == 0) {
			auto type = txtFilter->text();
			auto filter = service.filtrareType(type.toStdString());
			filterList->clear();
			for (auto& a : filter) {
				filterList->addItem(QString::fromStdString(a.getTitle()));

			}

		}
		else {
			auto description = txtFilter->text();
			filterList->clear();
			auto filter = service.filtrareDescriere(description.toStdString());
			for (auto& a : filter) {
				filterList->addItem(QString::fromStdString(a.getTitle()));

			}

		}

		});
	QObject::connect(sortComboBox, QOverload<int>::of(&QComboBox::activated), [&](int index) {
		if (sortComboBox->currentIndex() == 0) {
			auto sorted = service.sortByTitle();
			sortList->clear();
			for (auto& a : sorted) {
				sortList->addItem(QString::fromStdString(a.getTitle()));
			}

		}
		else if (sortComboBox->currentIndex()==1) {
			auto sorted = service.sortByTypeTime();
			sortList->clear();
			for (auto& a : sorted) {
				sortList->addItem(QString::fromStdString(a.getTitle()));
			}
		}
		else if (sortComboBox->currentIndex() == 2) {
			auto sorted = service.sortByDescription();
			sortList->clear();
			for (auto& a : sorted) {
				sortList->addItem(QString::fromStdString(a.getTitle()));
			}
		}
		});

	QObject::connect(btnSaveFilter, &QPushButton::clicked, [&]() {
		auto v = service.getAll();
		service.sterge_lista();
		for (int i = 0; i < filterList->count(); ++i)
		{
			
			QString str = filterList->item(i)->text();
			for (auto& a : v) {
				if (a.getTitle() == str.toStdString()) {
					service.addActivitate(a.getTitle(), a.getType(), a.getDescription(), a.getTime());
					break;
				}
			}
		}
		reloadList(service.getAll());

		
		});

	QObject::connect(btnSaveSort, &QPushButton::clicked, [&]() {
		auto v = service.getAll();
		service.sterge_lista();
		for (int i = 0; i < sortList->count(); ++i)
		{
			reloadList(service.getAll());
			QString str = sortList->item(i)->text();
			for (auto& a : v) {
				if (a.getTitle() == str.toStdString()) {
					service.addActivitate(a.getTitle(), a.getType(), a.getDescription(), a.getTime());
					break;
				}
			}
		}
		reloadList(service.getAll());


		});

	QObject::connect(btnSend, &QPushButton::clicked, [&]() {
		QMessageBox::warning(nullptr, "FAILED", "Sorry, our mail box is full!", QMessageBox::Ok);
		
		});

	QObject::connect(btnAddActivityToCurrentList, &QPushButton::clicked, [&]() {
		auto fereastra = new GuiActualList{ service };
		auto fereastra2 = new Painter{ service };
		fereastra->show();
		fereastra2->show();
		
		});


	QObject::connect(btnDetails, &QPushButton::clicked, [&]() {
		int nrRows = service.getAll().size();
		QTableWidget* tbl = new QTableWidget{ nrRows , 4 };
		auto fereastra = new QWidget{};
		auto vector = service.getAll();
		int i = 0;
		for (auto item : vector) {
			tbl->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(item.getTitle())));
			tbl->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(item.getType())));
			tbl->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(item.getDescription())));
			int number = item.getTime();
			string str_number = std::to_string(number);
			tbl->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(str_number)));
			i++;
		}
		auto layout = new QVBoxLayout{};
		layout->addWidget(tbl);
		fereastra->setLayout(layout);

		fereastra->show();


		});

}

void GUI::reloadList(const vector<Activity>& all) {
	mainList->clear();
	for (auto& a : all) {
		mainList->addItem(QString::fromStdString(a.getTitle()));
	}
}

void GUI::loadInfo(const string& filename) {
	std::ifstream in(filename);
	string t;
	additionalInformation->clear();

	while (std::getline(in, t)) {
		additionalInformation->addItem(QString::fromStdString(t));

	}
}

void GUI::closeEvent(QCloseEvent* event) {
	switch (QMessageBox(tr("Warning!"), tr("Are you sure you want to exit ?"), QMessageBox::Warning, QMessageBox::Yes | QMessageBox::Default, QMessageBox::No, QMessageBox::Escape).exec()) {
	case 3: {
		event->accept();
		break;
	}
	default: {
		event->ignore();
		break;
	}
	}
}