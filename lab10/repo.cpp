#include "repo.h"
#include "domain.h"
#include <assert.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <unordered_map>
#include <fstream>

using std::ostream;
using std::stringstream;
using std::unordered_map;

void FileRepository::loadAllFromFile() {
	std::ifstream fin(filename);
	if (!fin.is_open()) {
		throw RepositoryException("Could not open the file!");
	}
	string title, type, description;
	int time;
	while (!fin.eof()) {
		fin >> title >> type >> description >> time;
		Activity a{ title.c_str(),type.c_str(),description.c_str(),time };
		ListRepository::store(a);
	}
	fin.close();
}

void FileRepository::writeAllToFile() {
	std::ofstream fout(filename);
	if (!fout.is_open()) {
		throw RepositoryException("Could not open the file !");
	}
	vector<Activity>all = ListRepository::getAll();
	vector<Activity>::iterator it;
	for (it = all.begin(); it < all.end() - 1; it++) {
		fout << (*it).toString() << std::endl;
	}
	fout << (*(all.end() - 1)).toString();
	fout.close();
}


void ListRepository::empty_list() {
	list.clear();
}

void ListRepository::store(const Activity& a){

	if (exist(a)) {
		throw RepositoryException("Exista deja o activitate cu numele:" + a.getTitle());
	}
	list.push_back(a);
}



bool ListRepository::exist(const Activity& a) const {
	try {
		find(a.getTitle());
		return true;
	}
	catch (RepositoryException&) {
		return false;
	}
}
/*
Cauta
arunca exceptie daca nu exista activitate
*/
const Activity& ListRepository::find(string title) const {
	
	for (auto& a : list) {
		if (a.getTitle() == title)
			return a;
	}
	
	throw RepositoryException("Nu exista activitate cu numele:" + title);
}

Activity funct(std::pair<string, Activity> itr) {
	return itr.second;
}

const vector<Activity> ListRepository::getAll() noexcept {
	
	return list;

}

void ListRepository::modify(const Activity& a, const string& type, const string& description, const int time) {
	if (exist(a)) {
		for (auto& activ : list) {
			if(activ.getTitle()==a.getTitle())
			{ 
				activ.setType(type);
				activ.setDescription(description);
				activ.setTime(time);
				return;
			}
		}
	}
	else
		throw RepositoryException("Nu exista activitate cu numele:" + a.getTitle());
}


void ListRepository::to_delete(const Activity& a) {
	if (exist(a)) {
		
		vector<Activity>::iterator itr;
		for (itr = list.begin(); itr != list.end(); itr++) {
			if ((*itr).getTitle() == a.getTitle())
			{
				list.erase(itr);
				return;
			}
		}
	}
	else
		throw RepositoryException("Nu exista activitate cu numele:" + a.getTitle());
}

void FileRepository::empty_list() {
	ListRepository::empty_list();
	std::ofstream o(filename);
	o.close();
}


void FileRepository::store(const Activity& a)  {
	ListRepository::store(a);
	writeAllToFile();
}

void FileRepository::modify(const Activity& a, const string& type, const string& description, const int time) {
	ListRepository::modify(a, type, description, time);
	writeAllToFile();
}

void FileRepository::to_delete(const Activity& a) {
	ListRepository::to_delete(a);
	writeAllToFile();
}

ostream& operator<<(ostream& out, const RepositoryException& ex) {
	out << ex.message;
	return out;
}

void testAdauga() {
	ListRepository rep;
	rep.store(Activity{ "titlu","tip","descriere",4 });
	assert(rep.getAll().size() == 1);
	assert(rep.find("titlu").getTitle() == "titlu");

	rep.store(Activity{ "titlu1","tip1","descriere1",5 });
	assert(rep.getAll().size() == 2);


	try {
		rep.store(Activity{ "titlu","tip","descriere",23 });
		assert(false);
	}
	catch (const RepositoryException&) {
		assert(true);
	}
	//cauta inexistent
	try {
		rep.find("titlu2");
		assert(false);
	}
	catch (const RepositoryException& e) {
		stringstream os;
		os << e;
		assert(os.str().find("exista") >= 0);
	}
}

void testCauta() {
	ListRepository rep;
	rep.store(Activity{ "titlu","tip","descriere",4 });
	rep.store(Activity{ "titlu1","tip1","descriere1",4 });

	auto activ = rep.find("titlu");
	assert(activ.getTitle() == "titlu");
	assert(activ.getType() == "tip");

	//arunca exceptie daca nu gaseste
	try {
		rep.find("titlu2");
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
}


void testModify() {
	ListRepository rep;
	Activity a{ "ceva","gresit","de_modificat",4 };
	rep.store(Activity{ "ana","are","mere",1 });
	try {
		rep.modify(a, "tip_gresit", "gresit", 5);
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
	Activity a3{ "ana","are","mere",1 };
	rep.modify(a3, "mai_are_si", "pere", 2);
	Activity a2 = rep.find("ana");
	assert(a2.getTime() == 2);
	assert(a2.getDescription() == "pere");

}

void testStergeree() {
	ListRepository rep;
	Activity a{ "ana","are","mere",1 };
	Activity a1{ "mihai","n-are","mere",1 };
	rep.store(a);
	rep.store(a1);
	rep.to_delete(a);
	assert(rep.getAll().size() == 1);
	try {
		rep.to_delete(Activity{ "gigi","vine","si_cere",23 });
		assert(false);
	}
	catch (RepositoryException&) {
		assert(true);
	}
}


void testRandomize() {
	ListRepository rep;
	for (int i = 0; i < 1000; i++) {
		try {
			Activity a{ "activ","tip","descriere",i };
			rep.store(a);
			std::cout << "SUCCESS" << std::endl;
		}
		catch (RepositoryException& r)
		{
			std::cout << r << std::endl;
		}
	}
}

void testeRepo() {
	testAdauga();
	testCauta();
	testModify();
	testStergeree();
	testRandomize();
}

