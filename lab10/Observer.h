#pragma once
#include <vector>
#include <algorithm>


class Observer {
public:

	virtual void update() = 0;

};

class Observable {
protected:
	/*
	Non owning pointers to observer instances
	*/
	std::vector<Observer*> observers;

public:
	
	/*
	Adds an element to the main list
	obs - pointer to object of type Observer 
	*/
	void addObserver(Observer* obs) {
	
		observers.push_back(obs);
	
	}

	/*
	Deletes an element from the main list
	obs - pointr to object of type Observer
	*/
	void removeObserver(Observer* obs) {
	
		observers.erase(std::remove(observers.begin(), observers.end(), obs), observers.end());
	
	}
	
	/*
	Notifies every element from the main list to update
	*/
	void notify() {
	
		for (auto observer : this->observers) {
		
			observer->update();
		
		}
	}
};