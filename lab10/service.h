#pragma once
#include "domain.h"
#include "repo.h"
#include <string>
#include <vector>
#include "dto.h"
#include "undo.h"
#include <memory>
#include <functional>
#include "validator.h"
#include "listaActuala.h"

using std::vector;
using std::function;
using std::unique_ptr;

class Service: public Observable  {
private:

	Repository& rep;
	Validator& val;
	CurrentList& curr_list;
	// the memory will be released automatically
	vector<unique_ptr<UndoAction>>undoActions; 

public:

	Service(Repository& rep, Validator& val, CurrentList& curr_list) noexcept :rep{ rep }, val{ val }, curr_list{ curr_list } {}

	Service(const Service& ot) = delete;
	
	/*
	Returns all the activities in the same order they were added
	*/
	const vector<Activity> getAll() noexcept;

	/*
	Adds a new activity to the list
	parametres: title - string, type - string, description - string, time - integer
	Throws exception if the adding could not be done or is not valid
	*/
	void addActivitate(const string& title, const string& type, const string& description, int time);

	/*
	Sorteaza dupa descriere
	*/
	vector<Activity> sortByDescription();

	/*
	Sorteaza dupa titlu
	*/
	vector<Activity> sortByTitle();


	/*
	Sorteaza dupa tip+durata
	*/
	vector<Activity> sortByTypeTime();

	/*
	Filtreaza dupa o descriere data
	*/
	vector<Activity> filtrareDescriere(const string&);

	/*
	Filtreaza dupa un tip dat
	*/
	vector<Activity> filtrareType(const string& type);

	/*
	Modifica o descrierea si durata unei activitati date
	*/
	void modifyActivitate(const string& title, const string& type, const string& description, const int time);

	/*
	Cauta o activitate dupa titlu si tip
	*/
	const Activity findActivity(const string& title)const;

	/*
	Sterge o activitate din lista
	*/
	void deletee(const string& title);

	/*
	Returneaza ptin numele functiei un vector de rapoarte privind numarul de activitati de un anumit tip
	*/
	vector<DTO> rapoarte();

	/*
	Adauga o activitate in lista de activitati curente(activitate cautata in lista initiala dupa titlu)
	Date de intrare: title-string
	*/
	vector<Activity> addActivitateLista(const string& title);

	/*
	Goleste lista de activitati curente
	*/
	void goleste_lista();

	/*
	Genereaza o lista de activitati curente aleatoare din lista initiala de dimensiunea nr
	Date de intarre: nr-intreg
	*/
	int genereaza_lista(unsigned int numar);

	/*
	Returneaza activitatile din lista curenta
	*/
	const vector<Activity> get_all_list() noexcept {
		return curr_list.getAll();
	}

	/*
	Exporta lista de activitati curente in fisier
	*/
	void export_files(const string& filename);

	/*
	Reface ultima operatie facuta
	*/
	void undo();

	void sterge_lista() {
		rep.empty_list();
	}
	
	void addOsberver(Observer* obs) {
		curr_list.addObserver(obs);
	}

	void removeOsberver(Observer* obs) {
		curr_list.removeObserver(obs);
	}
};
void testCtr();
